package com.cerebralcore.ltetester;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by garyr on 3/21/2016.
 */
public class Test implements Parcelable {

    private int duration;
    private int running;

    // Getters
    public int getDuration(){
        return duration;
    }

    public boolean isRunning(){
        if(running==1){
            return true;
        }
        else
            return false;
    }

    // Setters
    public void stopTest(){
        running = 0;
    }

    // constructors
    public Test(int mduration){
        duration = mduration;
        running = 1;
    }

    public Test(int mduration, int mrunning){
        duration = mduration;
        running = mrunning;
    }

    // Parcelable Implemenation:
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(duration);
        out.writeInt(running);
    }

    private static Test readFromParcel(Parcel in ) {

        Integer duration = in.readInt();
        Integer running = in.readInt();
        return new Test(duration, running);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Test createFromParcel(Parcel in ) {
            return readFromParcel(in);
        }

        public Test[] newArray(int size) {
            return new Test[size];
        }
    };

}

