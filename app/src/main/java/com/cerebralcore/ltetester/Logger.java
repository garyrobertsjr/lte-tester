package com.cerebralcore.ltetester;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by garyr on 4/10/2016.
 */
public class Logger {
    private String fileName;
    FileWriter fw;
    Date d = new Date();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd,HH:mm:ss");
    SimpleDateFormat def = new SimpleDateFormat("yyyy-MM-dd_HHmmss");

    public void init(String name){
        if(name == null || name == ""){
            // Default naming schema
            //driveTest_date_time
            fileName = "driveTest_" + def.format(d) + ".csv";
        }
        else
            fileName = name + ".csv";

        // Define working directory
        File folder = new File(Environment.getExternalStorageDirectory()
                + "/ltetester");

        // Create if does not exist
        Boolean var;
        if (!folder.exists()) {
            var = folder.mkdir();
            Log.d("Dir", var.toString());
        }
        fileName = folder.toString() + "/" + fileName;
        try {
            File file = new File(fileName);
            fw = new FileWriter(file);

            fw.append("Date,Time,RSSI,PCI,MNC,LAT,LON,\n");

        }
        catch (IOException e){
            Log.d("IOError:", e.toString());
        }

    }

    public int entry(Integer rssi, Integer pci, Integer mnc, Double lat, Double lon){
     // write line to file
     // time, rssi, pci, mnc, lat, long
        d = new Date();
        try {
            Log.d("Entry:", sdf.format(d) + ','
                    + rssi.toString() + ','
                    + pci.toString() + ','
                    + mnc.toString() + ','
                    + lat.toString() + ','
                    + lon.toString() + ',' + '\n');

            fw.append(sdf.format(d) + ','
                    + rssi.toString() + ','
                    + pci.toString() + ','
                    + mnc.toString() + ','
                    + lat.toString() + ','
                    + lon.toString() + ',' + '\n');


            // OK
            return 0;
        }
        catch (IOException e){
            Log.d("IOError:", e.toString());
            // IO ERR
            return 1;
        }
    }

    public void close(){
        try {
            fw.close();
        }
        catch (IOException e){
            Log.d("IOError:", e.toString());
        }
    }

}
