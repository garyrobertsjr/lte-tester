package com.cerebralcore.ltetester;

import android.content.Context;
import android.telephony.CellInfo;
import android.telephony.CellInfoLte;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.util.List;

/**
 * Created by garyr on 2/29/2016.
 * Generic RIL interface class for accessing Telephony Manager LTE stats.
 * Needs to be instantiated with relevant context.
 */
public class RIL {
    Context mContext;
    TelephonyManager tm;
    List<CellInfo> cellInfoList;
    static int cellSig, cellID, cellMcc, cellMnc, cellPci, cellTac = 0;


    // Constructor:
    // Initiates telephony manager and defines context
    public RIL(Context mContext){
        this.mContext = mContext;
        tm = (TelephonyManager)mContext.getSystemService(mContext.TELEPHONY_SERVICE);
    }

    public int poll(){
        try{
            cellInfoList = tm.getAllCellInfo();
            for (CellInfo cellInfo : cellInfoList){
                if (cellInfo instanceof CellInfoLte) {

                    cellSig = ((CellInfoLte) cellInfo).getCellSignalStrength().getDbm();

                    // Gets the LTE cell indentity: (returns 28-bit Cell Identity, Integer.MAX_VALUE if unknown)
                    cellID = ((CellInfoLte) cellInfo).getCellIdentity().getCi();

                    // Gets the LTE MCC: (returns 3-digit Mobile Country Code, 0..999, Integer.MAX_VALUE if unknown)
                    cellMcc = ((CellInfoLte) cellInfo).getCellIdentity().getMcc();

                    // Gets the LTE MNC: (returns 2 or 3-digit Mobile Network Code, 0..999, Integer.MAX_VALUE if unknown)
                    cellMnc = ((CellInfoLte) cellInfo).getCellIdentity().getMnc();

                    // Gets the LTE PCI: (returns Physical Cell Id 0..503, Integer.MAX_VALUE if unknown)
                    cellPci = ((CellInfoLte) cellInfo).getCellIdentity().getPci();

                    // Gets the LTE TAC: (returns 16-bit Tracking Area Code, Integer.MAX_VALUE if unknown)
                    cellTac = ((CellInfoLte) cellInfo).getCellIdentity().getTac();

                    // Clean exit
                    return 1;
                }
            }
            // Improper instance
            return 2;
        }
        catch (Exception e){
            Log.d("SignalStrength", "++++++++ null array spot 2: " + e);

            //Error or malshaped parcel returned
            return 3;
        }
    }

    // Getters for LTE stats
    public int getCellSig(){return cellSig;}

    public int getCellPci(){return cellPci;}

    public int getCellID(){return cellID;}

    public int getCellMcc(){return cellMcc;}

    public int getCellTac(){return cellTac;}

    public int getCellMnc(){return cellMnc;}
}

