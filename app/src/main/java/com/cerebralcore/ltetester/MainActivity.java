package com.cerebralcore.ltetester;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    // Stat view items
    static String[] STAT_TYPES = new String[]{
            "RSSI", "PCI", "MNC", "Cell ID"
    };

    static int cellSig, cellID, cellMcc, cellMnc, cellPci, cellTac = 0;
    GridItemAdapter gridItemAdapter;
    GridView gridView;
    RIL ril;

    // Vars for updating UI via runnable thread
    int i = 0;
    final Handler myHandler = new Handler();

    private static Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Instantiate custom RIL interface
        ril = new RIL(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Launch test config when button is tapped.
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, TestConfigActivity.class);
                startActivity(intent);
            }
        });

        Integer test = 0;

        // Check Permissions
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, test);
        }

        // Create timer to update GUI from separate thread every 1s
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {UpdateGUI();}
        }, 0, 1000);

        // Instantiate grid view for each element in GridItemAdapter
        gridView = (GridView) findViewById(R.id.gridView);
        gridItemAdapter = new GridItemAdapter(this, STAT_TYPES);
        gridView.setAdapter(gridItemAdapter);
    }

    private void UpdateGUI() {
        i++;
        myHandler.post(myRunnable);
    }

    // Runnable thread object to be passed to timer
    final Runnable myRunnable = new Runnable() {
        public void run() {
            // Poll ril for new stats
            ril.poll();

            // Parse grid and update stats
            for(int i=0; i<gridView.getChildCount(); i++) {
                View grid_item = gridView.getChildAt(i);
                LinearLayout ll = (LinearLayout) grid_item;
                ImageView imageView = (ImageView) ll.getChildAt(1);
                TextView item_desc = (TextView) ll.getChildAt(2);

                ColorFilter redFilter = new LightingColorFilter( Color.BLACK, Color.RED);
                ColorFilter yellowFilter = new LightingColorFilter( Color.BLACK, Color.YELLOW);
                ColorFilter greenFilter = new LightingColorFilter( Color.BLACK, Color.GREEN);

                switch (item_desc.getId()) {
                    case R.id.rssi_val:
                        item_desc.setText(Integer.toString(ril.getCellSig()));

                        // Strong signal
                        if(ril.getCellSig() >= -115)
                            imageView.setColorFilter(greenFilter);
                            // OK signal
                        else if (ril.getCellSig() >= -130)
                            imageView.setColorFilter(yellowFilter);
                            // Weak signal
                        else
                            imageView.setColorFilter(redFilter);

                        break;

                    case R.id.pci_val:
                        if(cellPci != Integer.MAX_VALUE)
                            item_desc.setText(Integer.toString(ril.getCellPci()));
                        break;

                    case R.id.mnc_val:
                        if(cellMnc != Integer.MAX_VALUE)
                            item_desc.setText(Integer.toString(ril.getCellMnc()));
                        break;

                    case R.id.cellid_val:
                        if(cellID != Integer.MAX_VALUE)
                            item_desc.setText(Integer.toString(ril.getCellID()));
                        break;

                }
                //Log.d("THREAD", "" + item_desc.getText());
                ll.invalidate();

            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

