package com.cerebralcore.ltetester;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RelativeLayout;


public class TestConfigActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_config);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Define views in context
        final RelativeLayout duration_picker = (RelativeLayout) findViewById(R.id.duration_picker);
        final NumberPicker hour = (NumberPicker) findViewById(R.id.hour);
        final NumberPicker min = (NumberPicker) findViewById(R.id.min);
        final NumberPicker sec = (NumberPicker) findViewById(R.id.sec);

        // Set picker max values
        hour.setMaxValue(24);
        min.setMaxValue(60);
        sec.setMaxValue(60);

        // Hide Views until radio button event
        duration_picker.setVisibility(View.INVISIBLE);


        // Start test button
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create test object with duration.
                Test test = new Test(hour.getValue()*3600 + min.getValue()*60 + sec.getValue());
                // Start test activity
                Intent intent = new Intent(TestConfigActivity.this, TestActivity.class);
                // Place parcel Test obj into intent
                intent.putExtra("test", test);
                // Launch test activity
                startActivity(intent);
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    // Modifies Views based on radio button status
    public void onRadioButtonClicked(View view){
        boolean checked = ((RadioButton) view).isChecked();

        // Define main rl in context
        RelativeLayout duration_picker = (RelativeLayout) findViewById(R.id.duration_picker);

        // Reset visibility on click
        if(duration_picker.getVisibility() == View.VISIBLE) {
            duration_picker.setVisibility(View.INVISIBLE);
        }
        switch (view.getId()){
            case R.id.set_duration:
                if (checked) {
                    // Show numpicker
                    duration_picker.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.set_endless:
                if (checked)
                    // Nothing to do
                break;
        }
    }

}
