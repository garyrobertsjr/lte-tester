package com.cerebralcore.ltetester;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
/**
 * Created by garyr on 1/31/2016.
 */
public class GridItemAdapter extends BaseAdapter {
    private Context mContext;
    private final String[] stat_types;

    public GridItemAdapter(Context c, String[] stat_types) {
        this.mContext=c;
        this.stat_types=stat_types;
    }

    public int getCount() {
        return stat_types.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View gridView;

        if (convertView == null) {
            gridView = inflater.inflate(R.layout.stats, null);


            // Instantiate stat name label
            TextView stat_label = (TextView) gridView
                    .findViewById(R.id.grid_item_label);
            stat_label.setText(stat_types[position]);


            // Instantiate Stat Status Image
            final ImageView imageView = (ImageView) gridView
                    .findViewById(R.id.grid_item_image);

            String stat = stat_types[position];

            // Identify current layout for textView injection
            LinearLayout ll = (LinearLayout) gridView.findViewById(R.id.grid_item_layout);

            // Setup grid items for each stat.
            final TextView grid_detail = new TextView(mContext);
            grid_detail.setGravity(0x11);
            
            // Dummy Image until has RSSI value to process
            imageView.setImageResource(R.drawable.statusdot);

            ColorFilter redFilter = new LightingColorFilter( Color.BLACK, Color.RED);
            ColorFilter yellowFilter = new LightingColorFilter( Color.BLACK, Color.YELLOW);
            ColorFilter greenFilter = new LightingColorFilter( Color.BLACK, Color.GREEN);


            switch(stat){
                case "RSSI":
                    ll.setId(R.id.rssi_ll);
                    grid_detail.setText(Integer.toString(MainActivity.cellSig));
                    grid_detail.setId(R.id.rssi_val);

                    // Strong signal
                    if(MainActivity.cellSig >= -70)
                        imageView.setColorFilter(greenFilter);

                    // OK signal
                    else if (MainActivity.cellMnc >= -90)
                        imageView.setColorFilter(yellowFilter);
                    // Weak signal
                    else
                        imageView.setColorFilter(redFilter);
                    break;

                case "PCI":
                    grid_detail.setId(R.id.pci_val);
                    grid_detail.setText(Integer.toString(MainActivity.cellPci));
                    imageView.setImageResource(R.drawable.radio48);
                    break;

                case "MNC":

                    Integer temp = MainActivity.cellMnc;

                    if(temp == Integer.MAX_VALUE) {
                        grid_detail.setText("UNK");
                    }
                    else {
                        grid_detail.setText(Integer.toString(temp));
                    }
                    grid_detail.setId(R.id.mnc_val);
                    break;

                case "Cell ID":
                    grid_detail.setId(R.id.cellid_val);

                    temp = MainActivity.cellID;

                    if(temp == Integer.MAX_VALUE) {
                        grid_detail.setText("UNK");
                    }
                    else {
                        grid_detail.setText(Integer.toString(temp));
                    }
                    break;

                case "LAT":
                    grid_detail.setId(R.id.lat_val);
                    imageView.setImageResource(R.drawable.latitude);
                    break;

                case "LON":
                    grid_detail.setId(R.id.lon_val);
                    imageView.setImageResource(R.drawable.longitude);
                    break;

                default:
                    grid_detail.setText("000");
                    break;

            }
            ll.addView(grid_detail);

        } else {
            gridView = (View) convertView;
        }


        return gridView;
    }

}

