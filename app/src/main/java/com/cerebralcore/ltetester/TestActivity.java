package com.cerebralcore.ltetester;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Timer;
import java.util.TimerTask;

public class TestActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener{

    // Vars for updating UI via runnable thread
    int i = 0;
    final Handler myHandler = new Handler();
    GoogleApiClient mGoogleApiClient;
    RIL ril;
    Location mlocation;
    LocationRequest mLocationRequest;
    GridItemAdapter gridItemAdapter;
    GridView testGrid;
    ProgressBar progressBar;
    boolean stop_flag = false;
    static GoogleMap map;

    Test test;
    Timer timer;
    Logger logger;
    int elapsed = 0;

    // Stat view items
    static String[] TEST_STAT_TYPES = new String[]{
            "RSSI", "PCI", "LAT", "LON"
    };
    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    // Update cached location value
    @Override
    public void onLocationChanged(Location location) {
        mlocation = location;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        ril = new RIL(this);
        logger = new Logger();
        test = getIntent().getExtras().getParcelable("test");

        // Check Permissions
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }

        // Need to add name field to Test and use here
        logger.init("");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // Create an instance of GoogleAPIClient.

        if (mGoogleApiClient == null) {
            // ATTENTION: This "addApi(AppIndex.API)"was auto-generated to implement the App Indexing API.
            // See https://g.co/AppIndexing/AndroidStudio for more information.
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(AppIndex.API).build();
        }

        // Configure polling rates/accuracy
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Check current settings
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());

        // Create timer to poll GPS and RIL, log metrics, then update GUI from separate thread every 1s
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                UpdateGUI();
            }
        }, 0, 1000);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                stop_flag = true;
            }
        });



    }


    // Connect to GPlay API at start of activity
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Test Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.cerebralcore.ltetester/http/host/path")
        );
        AppIndex.AppIndexApi.start(mGoogleApiClient, viewAction);
    }

    // Disconnect from GPlay API when exiting activity
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Test Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.cerebralcore.ltetester/http/host/path")
        );
        AppIndex.AppIndexApi.end(mGoogleApiClient, viewAction);
    }

    // Callable for tick/sampling event.
    private void UpdateGUI() {
        i++;
        myHandler.post(myRunnable);
    }

    // Runnable to poll RIL
    // poll -> log -> update GUI
    final Runnable myRunnable = new Runnable() {
        @Override
        public void run() {

            // Halt, update UI, and end logging for premature stop.
            if(stop_flag){
                test.stopTest();
                timer.cancel();
                logger.close();
                progressBar.setVisibility(View.INVISIBLE);
                ((TextView)findViewById(R.id.timer_hud)).setText("STOPPED");
                return;
            }

            ril.poll();
            elapsed += 1;
            // DEBUG
            Log.d("TESTING", "RSSI: " + ((Integer) ril.getCellSig()).toString());
            if(mlocation != null) {
                Log.d("TESTING", "Lat: " + ((Double) mlocation.getLatitude()).toString());
                Log.d("TESTING", "Lon: " + ((Double) mlocation.getLongitude()).toString());
                Log.d("TESTING", "Dur: " + ((Integer) test.getDuration()).toString());

                //Log metrics
                logger.entry(
                        ril.getCellSig(),
                        ril.getCellPci(),
                        ril.getCellMnc(),
                        mlocation.getLatitude(),
                        mlocation.getLongitude());
            }

            // Update GUI clock
            ((TextView)findViewById(R.id.timer_hud)).setText(
                    DateUtils.formatElapsedTime(test.getDuration()-elapsed));

            // Update Grid Items Here
            testGrid = (GridView) findViewById(R.id.testGrid);
            for(int i=0; i<testGrid.getChildCount(); i++) {
                View grid_item = testGrid.getChildAt(i);
                LinearLayout ll = (LinearLayout) grid_item;
                ImageView imageView = (ImageView) ll.getChildAt(1);
                TextView item_desc = (TextView) ll.getChildAt(2);

                ColorFilter redFilter = new LightingColorFilter( Color.BLACK, Color.RED);
                ColorFilter yellowFilter = new LightingColorFilter( Color.BLACK, Color.YELLOW);
                ColorFilter greenFilter = new LightingColorFilter( Color.BLACK, Color.GREEN);

                switch (item_desc.getId()) {
                    case R.id.rssi_val:
                        item_desc.setText(Integer.toString(ril.getCellSig()));

                        // Strong signal
                        if(ril.getCellSig() >= -115)
                            imageView.setColorFilter(greenFilter);
                            // OK signal
                        else if (ril.getCellSig() >= -130)
                            imageView.setColorFilter(yellowFilter);
                            // Weak signal
                        else
                            imageView.setColorFilter(redFilter);

                        break;

                    case R.id.pci_val:
                        item_desc.setText(Integer.toString(ril.getCellPci()));
                        break;

                    case R.id.lat_val:
                        if(mlocation != null)
                            item_desc.setText(Double.toString(mlocation.getLatitude()));
                        break;

                    case R.id.lon_val:
                        if(mlocation != null)
                            item_desc.setText(Double.toString(mlocation.getLongitude()));
                        break;

                }
                //Log.d("THREAD", "" + item_desc.getText());
                ll.invalidate();


            }
            // Plot blip on Map
            if(map != null && mlocation!= null){
                float colorIndicator = BitmapDescriptorFactory.HUE_YELLOW;
                // Determine marker color
                if (ril.getCellSig() < -110){
                    colorIndicator = BitmapDescriptorFactory.HUE_RED;
                }
                else if(ril.getCellSig() >= -100) {
                    colorIndicator = BitmapDescriptorFactory.HUE_GREEN;
                }

                // Add marker
                map.addMarker(new MarkerOptions().position(
                        new LatLng(
                                mlocation.getLatitude(),
                                mlocation.getLongitude())).title(
                                    ((Integer) ril.getCellSig()).toString())
                                .icon(BitmapDescriptorFactory.defaultMarker(colorIndicator)));
            }

            // Progress Bar
            progressBar = (ProgressBar) findViewById(R.id.progressBar);
            progressBar.setMax(test.getDuration());
            progressBar.setProgress(elapsed/test.getDuration());

            // Log Entry
            if(elapsed == test.getDuration() && test.isRunning()){
                test.stopTest();
                timer.cancel();
                logger.close();
                progressBar.setVisibility(View.INVISIBLE);
            }
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_test, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        startLocationUpdates();
    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        //skip
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //skip
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_test, container, false);
            return rootView;
        }
    }



    // Fragment object for test overview
    public static class TestFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public TestFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static TestFragment newInstance(int sectionNumber) {
            TestFragment fragment = new TestFragment();
            Bundle args = new Bundle();
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Load TestOverview Fragment Layout
            View rootView = inflater.inflate(R.layout.fragment_test, container, false);

            // Instantiate grid view for each element in GridItemAdapter
            GridView testGrid = (GridView) rootView.findViewById(R.id.testGrid);
            GridItemAdapter gridItemAdapter = new GridItemAdapter(getActivity(), TEST_STAT_TYPES);
            testGrid.setAdapter(gridItemAdapter);

            // Instantiate Progress Bar
            ProgressBar progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
            progressBar.setProgress(0);
            return rootView;
        }
    }

    // Fragment object for test overview
    public static class MapViewFragment extends Fragment implements OnMapReadyCallback{
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public MapViewFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static MapViewFragment newInstance() {
            MapViewFragment fragment = new MapViewFragment();
            Bundle args = new Bundle();
            fragment.setArguments(args);
            return fragment;
        }
        @Override public void onMapReady(GoogleMap googleMap){
            googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
            googleMap.setMyLocationEnabled(true);

            map = googleMap;
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Load TestOverview Fragment Layout
            View rootView = inflater.inflate(R.layout.fragment_map, container, false);

            SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);

            return rootView;
        }
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            switch (position){
                case 0:
                    // Test Overview fragment
                    return TestFragment.newInstance(0);
                case 1:
                    return MapViewFragment.newInstance();
                default:
                    // Something went wrong...
                    return PlaceholderFragment.newInstance(3);
            }

        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Test Overview";
                case 1:
                    return "Graph View";
                case 2:
                    return "Map View";
            }
            return null;
        }
    }
}
